import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import axios from 'axios';
import store from '../store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/app',
    name: 'App',
    component: () => import(/* webpackChunkName: "app-main" */ '../views/App.vue'),
    redirect: '/app/users',
    children: [
      {
        path: 'live',
        component: () => import(/* webpackChunkName: "live" */ '../components/CardList__live.vue'),
      },
      {
        path: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue'),
      },
      {
        path: 'wiki',
        component: () => import(/* webpackChunkName: "profile" */ '../views/Wiki.vue'),
      },
      {
        path: 'users',
        component: () => import(/* webpackChunkName: "users" */ '../components/Userlist__admin.vue'),
      },
      {
        path: 'reports',
        component: () => import(/* webpackChunkName: "reports" */ '../views/Reports.vue'),
      },
      {
        path: 'configs',
        component: () => import(/* webpackChunkName: "configs" */ '../views/Configs.vue'),
      }
    ],
    beforeEnter(to, from, next) {
      const config = {
        method: 'get',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/validateAdmin`,
        withCredentials: true,
      }
      axios(config)
      .then(res => {
        store.commit('setDataUser', res.data);
        next();
      }).catch(error => {
        next('/login');
      });
    },
  },
  {
    path: "*",
    beforeEnter(to, from, next){
      next('/');
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;

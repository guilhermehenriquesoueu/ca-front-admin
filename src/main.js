import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);

Vue.config.productionTip = false;

require("./assets/css/main.scss");

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    firstName: '',
    secondName: '',
    email: '',
    whatsapp: '',
    telegram: '', 
    sessionId: '',
    nextPayment: '',
    matches: '',
    favoriteMatches: [],
    usersList: '',
    pId: '',
  },
  getters: {
    getUserInfo: (state) => state,
  },
  mutations: {
    setDataUser(state, newState){
      const { firstName, secondName, email, whatsapp, telegram, sessionId, nextPayment, pId } = newState;
      state.firstName = firstName;
      state.secondName = secondName;
      state.email = email;
      state.whatsapp = whatsapp;
      state.telegram = telegram;
      state.sessionId = sessionId;
      state.nextPayment = nextPayment;
      state.pId = pId;
    },
    setWhatsapp(state, newState){
      state.whatsapp = newState;
    },
    setTelegram(state, newState){
      state.telegram = newState;
    },
    setMatches(state, newState){
      state.matches = newState;
    },
    setFavoriteMatch(state, elementId){
      state.favoriteMatches.unshift(elementId);
    },
    setUsersList(state, newState){
      state.usersList = newState;
    },
    removeFavoriteMatch(state, elementId){
      const favoriteMatch = state.favoriteMatches.find(el =>  el === elementId);
      const indexFavoriteMatch = state.favoriteMatches.indexOf(favoriteMatch);
      if(indexFavoriteMatch > -1){
        state.favoriteMatches.splice(indexFavoriteMatch, 1);
      }
    },
  },
  actions: {
    updateProfile({commit, state}, data) {
      const { whatsapp, telegram } = data;
      const email = state.email;
      const newData = JSON.stringify({ email, whatsapp, telegram });
      const config = {
        method: 'put',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/user`,
        headers: {
          'Content-Type': 'application/json',
        },
        data: newData,
        withCredentials: true,
      };
      return new Promise ((resolve, reject) => {
        axios(config).then( (res) => {
          commit('setWhatsapp', whatsapp);
          commit('setTelegram', telegram);
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    resetPassword({}, data){
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/password/`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
        withCredentials: true,
      };
      return new Promise ((resolve, reject) => {
        axios(config).then((res) => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    resetPasswordLogin({}, data){
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/password/`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
        withCredentials: true,
      };
      return new Promise((resolve, reject) => {
        axios(config).then((res) => {
        resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    changePassword({}, data){
      const { email, token, ...rest } = data;
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/password/${email}/${token}`,
        headers: {
          'Content-Type': 'application/json',
        },
        data: rest,
        withCredentials: true,
      };
      return new Promise((resolve, reject) => {
        axios(config).then((res) => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    login({}, data){
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/loginAdmin`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
        withCredentials: true,
      };
      return new Promise((resolve, reject) => {
        axios(config).then((res) => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    getConfig({}, data){
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/getallconfigs`,
        headers: {
          'Content-Type': 'application/json', 
        },
        data,
        withCredentials: true,
      }
      return new Promise((resolve, reject) => {
        axios(config).then(res => {
          resolve(res.data.data);
        }).catch(() => {
          return;
        });
      });
    },
    setConfig({}, data){
      const config = {
        method: 'put',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/config`,
        headers: {
          'Content-Type': 'application/json', 
        },
        data,
        withCredentials: true,
      }
      return new Promise((resolve, reject) => {
        axios(config).then(res => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    updateConfig({}, data){
      const config = {
        method: 'patch',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/config`,
        headers: {
          'Content-Type': 'application/json', 
        },
        data,
        withCredentials: true,
      }
      return new Promise((resolve, reject) => {
        axios(config).then(res => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    },
    changePaymentStatus({}, data) {
      const config = {
        method: 'put',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/payment`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
        withCredentials: true
      };
      return new Promise ((resolve, reject) => {
        axios(config).then( (res) => {
          if(res.data.status === false) resolve (res.data.status);
          resolve (res.data.status);
        });
      });
    },
    getUsersList({commit, state}, data){
      const config = {
        method: 'post',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/users`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
        withCredentials: true
      };
      return new Promise ((resolve, reject) => {
        axios(config).then( (res) => {
          if(res.data.status === false) resolve (res.data.status);
          commit('setUsersList', res.data.data);
          resolve (res.data.status);
        });
      });
    },
    disconnect({}){
      const config = {
        method: 'get',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/destroySession`,
        withCredentials: true,
      };
      return new Promise((resolve, reject) => {
        axios(config).then((res) => {
          resolve(res.data);
        }).catch(() => {
          return;
        });
      });
    }
  }
});
